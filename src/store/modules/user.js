

import firebase from 'firebase'
require('firebase/auth')

export default {
    state: {
        user: {
            isAuthenticated: false,
            userId: null
        },
        isPopup: false
    },
    mutations: {
        setUser(state, payload) {
            state.user.isAuthenticated = true
            state.user.userId = payload
        },
        unSetUser(state) {
            state.user = {
                isAuthenticated: false,
                userId: null
            }
        }
    },
    actions: {
        signUp({commit}, payload) {
            console.log(payload)
            commit('setProcessing', true)
            commit('unSetUser')
            firebase.auth().createUserWithEmailAndPassword(payload.email, payload.password)
            .then(user => {
                console.log(user)
                commit('setProcessing', true)
                commit('setUser', user.uid)

            })
            .catch(function(error) {
                // Handle Errors here.
                console.log(error)
                commit('setProcessing', false)
                commit('setError', error.message)
                //let errorCode = error.code;
                //let errorMessage = error.message;
                // ...
            })
        },
        stateChange({commit}, payload) {
            if(payload) {
                commit('setUser', payload.uid)
            } 
            else {
                commit('unSetUser')
            }
        }
    },
    getters: {
        isUserAuthentication: state => state.user.isAuthenticated,
        isPopup: state => state.isPopup
    }
}